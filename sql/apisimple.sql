-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb2+deb7u2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 08-01-2016 a las 18:05:45
-- Versión del servidor: 5.5.46
-- Versión de PHP: 5.4.45-0+deb7u2

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `apisimple`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nota`
--

CREATE TABLE IF NOT EXISTS `nota` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ID_USUARIO` int(11) NOT NULL,
  `TEXTO` varchar(140) NOT NULL,
  `FAVORITA` tinyint(1) NOT NULL DEFAULT '0',
  `FECHA` datetime NOT NULL,
  PRIMARY KEY (`ID`,`ID_USUARIO`),
  KEY `FECHA` (`FECHA`),
  KEY `ID_USUARIO` (`ID_USUARIO`,`FAVORITA`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Volcado de datos para la tabla `nota`
--

INSERT INTO `nota` (`ID`, `ID_USUARIO`, `TEXTO`, `FAVORITA`, `FECHA`) VALUES
(1, 1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.', 0, '2016-01-03 20:21:41'),
(2, 1, 'Duis nec elementum tellus, sed egestas tortor.', 1, '2016-01-03 20:21:41'),
(3, 1, 'Integer nec neque sit amet quam euismod porttitor in sit amet ligula.', 1, '2016-01-03 20:22:24'),
(4, 1, 'Duis auctor ante vel dapibus consequat. Nunc efficitur libero vel pharetra sodales. Donec pellentesque nunc libero.', 0, '2016-01-03 20:22:24'),
(5, 1, 'Suspendisse lacus elit, sodales sed elit a, finibus ultricies sem. Vivamus ac odio eros.', 1, '2016-01-03 23:06:05'),
(6, 1, 'Proin bibendum efficitur diam, interdum consequat diam hendrerit in.', 0, '2016-01-03 23:06:05'),
(7, 1, 'Cras a lacus eget est tincidunt elementum.', 0, '2016-01-03 23:27:17'),
(8, 1, 'Nunc sit amet dignissim lorem. Duis faucibus velit felis, dapibus suscipit nunc consequat quis.', 0, '2016-01-03 23:29:49'),
(9, 1, 'Fusce vel ornare ante', 0, '2016-01-03 23:32:26'),
(10, 1, 'Donec id dui mauris.', 0, '2016-01-03 23:32:38'),
(11, 1, 'Pellentesque accumsan sapien sit amet risus venenatis euismod.', 0, '2016-01-03 23:32:41'),
(12, 1, 'Pellentesque ac dapibus purus, et congue ipsum.', 0, '2016-01-08 18:00:12');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`ID`) VALUES
(1),
(2),
(3),
(4);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
