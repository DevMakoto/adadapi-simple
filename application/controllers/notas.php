<?php
/**
 * This controller class loads simple named pages like "home", ppp pages etc.
 *
 * @author Pedro Escudero
 */
class Notas extends CI_Controller {
    
    public function __construct(){
        parent::__construct();
        
        // Define a global variable to store data that is then used by the end view page.
        $this->data = null;
    }
    
    public function index(){
        $this->load->view('notas_view');
    }
    
    public function crear(){
        if ($this->input->post('id_usuario')) {
            $id_usuario = $this->input->post('id_usuario');
            $texto = $this->input->post('texto');
            
            $this->load->model('apisimple_model');
            
            $resultado = $this->apisimple_model->insert_nota($id_usuario, $texto);
            
            $this->load->dbutil();
            $config = array (
                'root'    => 'respuesta',
                'element' => 'nota',
                'newline' => "\n",
                'tab'     => "\t"
            );
            
            $this->output->set_content_type('application/xml');
            $this->output->set_output($this->dbutil->xml_from_result($resultado, $config));
        } else {
            $this->load->view('crear_view');
        }
    }
    
    public function consultar(){
        if ($this->input->post('id_usuario')) {
            $inicial = $this->input->post('inicial')?$this->input->post('inicial'):0;
            $cantidad = $this->input->post('cantidad')?$this->input->post('cantidad'):10;
            $id_usuario = $this->input->post('id_usuario');
            $id_nota = $this->input->post('id_nota');
            
            $this->load->model('apisimple_model');
            
            if($id_nota) {
                $resultado = $this->apisimple_model->get_nota_by_id($id_usuario, $id_nota);
            } else {
                $resultado = $this->apisimple_model->get_notas($id_usuario, $inicial, $cantidad);
            }
            
            $this->load->dbutil();
            $config = array (
                'root'    => 'respuesta',
                'element' => 'nota',
                'newline' => "\n",
                'tab'     => "\t"
            );
            
            $this->output->set_content_type('application/xml');
            $this->output->set_output($this->dbutil->xml_from_result($resultado, $config));
        } else {
            $this->load->view('consultar_view');
        }
    }
    
    public function favoritas(){
        if ($this->input->post('id_usuario')) {
            $inicial = $this->input->post('inicial')?$this->input->post('inicial'):0;
            $cantidad = $this->input->post('cantidad')?$this->input->post('cantidad'):10;
            $id_usuario = $this->input->post('id_usuario');
            $id_nota = $this->input->post('id_nota');
            
            $this->load->model('apisimple_model');
            
            if($id_nota) {
                $resultado = $this->apisimple_model->set_nota_favorita($id_usuario, $id_nota);
            } else {
                $resultado = $this->apisimple_model->get_notas_favoritas($id_usuario, $inicial, $cantidad);
            }
            
            $this->load->dbutil();
            $config = array (
                'root'    => 'respuesta',
                'element' => 'nota',
                'newline' => "\n",
                'tab'     => "\t"
            );
            
            $this->output->set_content_type('application/xml');
            $this->output->set_output($this->dbutil->xml_from_result($resultado, $config));
        } else {
            $this->load->view('favoritas_view');
        }
    }
}

?>