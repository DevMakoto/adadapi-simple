<?php
/**
 * This controller class loads simple named pages like "home", ppp pages etc.
 *
 * @author Pedro Escudero
 */
class Controladores extends CI_Controller {
    
    public function __construct(){
        parent::__construct();
        
        // Define a global variable to store data that is then used by the end view page.
        $this->data = null;
        
    }
    
    function consultar() {
        if( function_exists('curl_init') && function_exists('curl_exec') ) {
        
            if( !ini_get('safe_mode') ){
                set_time_limit(2 * 60);
            }
            
            $method = 'POST';
            $url = site_url('notas/consultar');
            $fields['id_usuario'] = '1';
            
            //$inicial = $this->input->post('inicial')?$this->input->post('inicial'):0;
            //$cantidad = $this->input->post('cantidad')?$this->input->post('cantidad'):10;
            //$id_usuario = $this->input->post('id_usuario');
            //$id_nota = $this->input->post('id_nota');
            
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);

            curl_setopt($ch, CURLOPT_POST, $method == 'POST');

            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2 * 60 * 1000);

            $response   = curl_exec($ch);
            $info       = curl_getinfo($ch);
            $error      = curl_error($ch);
                
            curl_close($ch);
            
            $this->data['respuesta'] = $response;
            $this->data['error'] = $error;
            $this->data['info'] = $info;
            
            $this->load->view('controladores/controladores_view', $this->data);
        }
    }
    
    function crear() {
        if( function_exists('curl_init') && function_exists('curl_exec') ) {
        
            if( !ini_get('safe_mode') ){
                set_time_limit(2 * 60);
            }
            
            $method = 'POST';
            $url = site_url('notas/crear');
            $fields['id_usuario'] = '1';
            $lorem_ipsum = simplexml_load_file('http://www.lipsum.com/feed/xml?amount=1&what=paras&start=0')->lipsum;
            $texto = strstr($lorem_ipsum, '.', true).'.';
            $fields['texto'] = $texto;
            
            //$inicial = $this->input->post('inicial')?$this->input->post('inicial'):0;
            //$cantidad = $this->input->post('cantidad')?$this->input->post('cantidad'):10;
            //$id_usuario = $this->input->post('id_usuario');
            //$id_nota = $this->input->post('id_nota');
            
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);

            curl_setopt($ch, CURLOPT_POST, $method == 'POST');

            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2 * 60 * 1000);

            $response   = curl_exec($ch);
            $info       = curl_getinfo($ch);
            $error      = curl_error($ch);
                
            curl_close($ch);
            
            $this->data['respuesta'] = $response;
            $this->data['error'] = $error;
            $this->data['info'] = $info;
            
            $this->load->view('controladores/controladores_view', $this->data);
        }
    }
    
    function favoritas() {
        if( function_exists('curl_init') && function_exists('curl_exec') ) {
        
            if( !ini_get('safe_mode') ){
                set_time_limit(2 * 60);
            }
            
            $method = 'POST';
            $url = site_url('notas/favoritas');
            $fields['id_usuario'] = '1';
            
            //$inicial = $this->input->post('inicial')?$this->input->post('inicial'):0;
            //$cantidad = $this->input->post('cantidad')?$this->input->post('cantidad'):10;
            //$id_usuario = $this->input->post('id_usuario');
            //$id_nota = $this->input->post('id_nota');
            
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);

            curl_setopt($ch, CURLOPT_POST, $method == 'POST');

            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2 * 60 * 1000);

            $response   = curl_exec($ch);
            $info       = curl_getinfo($ch);
            $error      = curl_error($ch);
                
            curl_close($ch);
            
            $this->data['respuesta'] = $response;
            $this->data['error'] = $error;
            $this->data['info'] = $info;
            
            $this->load->view('controladores/controladores_view', $this->data);
        }
    }
}

?>