<?php
/**
 * This controller class loads simple named pages like "home", ppp pages etc.
 *
 * @author Pedro Escudero
 */
class Modelos extends CI_Controller {
    
    public function __construct(){
        parent::__construct();
        
        // Define a global variable to store data that is then used by the end view page.
        $this->data = null;
        
        $this->load->library('unit_test');
        $this->load->model('apisimple_model');
    }
    
    public function get_notas() {
        $test_name = 'Prueba función get_notas($id_usuario, $inicial = 0, $cantidad = 10) del modelo';
        $id_usuario = '1';
        $cantidad = '30';
        $inicial = '1';
        $ejecucion = $this->apisimple_model->get_notas($id_usuario, $inicial, $cantidad)->result_array();
        $this->unit->run($ejecucion, 'is_array', $test_name);
        $resultado = $this->unit->result();
        $this->data['resultados_test'] = $resultado;
        $this->data['last_query'] = $this->db->last_query();
        $this->data['datos_ejecucion'] = $ejecucion;
        $this->load->view('test_view', $this->data);
    }
    
    public function get_nota_by_id() {
        $test_name = 'Prueba función get_nota_by_id($id_usuario, $id_nota)';
        $id_usuario = '1';
        $id_nota = '2';
        $ejecucion = $this->apisimple_model->get_nota_by_id($id_usuario, $id_nota)->result_array();
        $this->unit->run($ejecucion, 'is_array', $test_name);
        $resultado = $this->unit->result();
        $this->data['resultados_test'] = $resultado;
        $this->data['last_query'] = $this->db->last_query();
        $this->data['datos_ejecucion'] = $ejecucion;
        $this->load->view('test_view', $this->data);
    }
    
    public function get_notas_favoritas() {
        $test_name = 'Prueba función get_notas_favoritas($id_usuario, $inicial = 0, $cantidad = 10)';
        $id_usuario = '1';
        $cantidad = '30';
        $inicial = '1';
        $ejecucion = $this->apisimple_model->get_notas_favoritas($id_usuario, $inicial, $cantidad)->result_array();
        $this->unit->run($ejecucion, 'is_array', $test_name);
        $resultado = $this->unit->result();
        $this->data['resultados_test'] = $resultado;
        $this->data['last_query'] = $this->db->last_query();
        $this->data['datos_ejecucion'] = $ejecucion;
        $this->load->view('test_view', $this->data);
    }
    
    public function set_nota_favorita() {
        $test_name = 'Prueba función set_nota_favorita($id_usuario, $id_nota)';
        $id_usuario = '1';
        $id_nota = '3';
        $ejecucion = $this->apisimple_model->set_nota_favorita($id_usuario, $id_nota)->result_array();
        $this->unit->run($ejecucion, 'is_array', $test_name);
        $resultado = $this->unit->result();
        $this->data['resultados_test'] = $resultado;
        $this->data['last_query'] = $this->db->last_query();
        $this->data['datos_ejecucion'] = $ejecucion;
        $this->load->view('test_view', $this->data);
    }
    
    public function insert_nota() {
        $test_name = 'Prueba función insert_nota($id_usuario, $texto)';
        $lorem_ipsum = simplexml_load_file('http://www.lipsum.com/feed/xml?amount=1&what=paras&start=0')->lipsum;
        $texto = strstr($lorem_ipsum, '.', true).'.';
        $id_usuario = '1';
        $ejecucion = $this->apisimple_model->insert_nota($id_usuario, $texto)->result_array();
        $this->unit->run($ejecucion, 'is_array', $test_name);
        $resultado = $this->unit->result();
        $this->data['resultados_test'] = $resultado;
        $this->data['last_query'] = $this->db->last_query();
        $this->data['datos_ejecucion'] = $ejecucion;
        $this->load->view('test_view', $this->data);
    }
}

?>