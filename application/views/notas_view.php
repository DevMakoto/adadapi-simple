<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="Este es un API de ejemplo desarrollada solo con la intención de tener algo de código que mostrar.">
        <meta name="author" content="">
        <link rel="icon" href="/resources/style/favicon.ico">

        <title>API simple de ejemplo</title>
        
        <!-- CSS Reset -->
        <link href="/resources/style/css/reset.css" rel="stylesheet">

        <!-- Bootstrap core CSS -->
        <link href="/resources/style/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="/resources/style/css/style.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- <link rel="stylesheet" type="text/css" href="print.css" media="print" /> -->
    </head>

    <body>
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <?php echo anchor('/', 'API simple de ejemplo', array('class'=>'navbar-brand')) ?>
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                </div>

                <div class="collapse navbar-collapse" id="bs-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><?php echo anchor(base_url().'#section-licencia', 'Licencia')?></li>
                        <li><?php echo anchor('http://pedroescudero.info', 'Contacto', array('target'=>'portfolio')) ?></li>
                    </ul>
                </div>
            </div>
        </nav>

        <article>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1>API simple de ejemplo</h1>
                        <section>
                        <h2>Acerca de</h2>
                            <p>Este es un API de ejemplo desarrollada solo con la intención de tener algo de código que mostrar. Tras el telón está el framework PHP CodeIgniter y utiliza una base de datos SQL sencilla. Para generar esta interfaz he utilizado Bootstrap. El código del API puede descargarse desde <?php echo anchor('https://bitbucket.org/escuderopedro/', 'mi perfil de Bitbucket', array('target'=>'bitbucket')) ?>.</p>
                        </section>
                        <section>
                            <h2>API</h2>
                            <p>El API simula la funcionalidad básica de una aplicación tipo Twitter. Permite a usuarios crear notas, consultarlas en conjunto o de manera individual y marcar notas como favoritas.</p>
                            <ul>
                                <li><?php echo anchor('notas/crear', 'Crear notas') ?></li>
                                <li><?php echo anchor('notas/consultar', 'Consultar notas') ?></li>
                                <li><?php echo anchor('notas/favoritas', 'Notas favoritas') ?></li>
                            </ul>
                        </section>
                        <section>
                            <h2>Pruebas</h2>
                            <p>Listado rápido de acceso a las pruebas para los distintos modelos y librerías.</p>
                            <section>
                                <h3>Modelos</h3>
                                <section>
                                    <h4 class="code-lvl-1">apisimple_model</h4>
                                    <ul>
                                        <li class="code-lvl-2"><?php echo anchor('test/modelos/get_notas', 'get_notas')?></li>
                                        <li class="code-lvl-2"><?php echo anchor('test/modelos/get_nota_by_id', 'get_nota_by_id')?></li>
                                        <li class="code-lvl-2"><?php echo anchor('test/modelos/get_notas_favoritas', 'get_notas_favoritas')?></li>
                                        <li class="code-lvl-2"><?php echo anchor('test/modelos/set_nota_favorita', 'set_nota_favorita')?></li>
                                        <li class="code-lvl-2"><?php echo anchor('test/modelos/insert_nota', 'insert_nota')?></li>
                                    </ul>
                                </section>
                            </section>
                            <section>
                                <h3>Controladores</h3>
                                <section>
                                    <h4 class="code-lvl-1">notas</h4>
                                    <ul>
                                        <li class="code-lvl-2"><?php echo anchor('test/controladores/consultar', 'consultar')?></li>
                                        <li class="code-lvl-2"><?php echo anchor('test/controladores/crear', 'crear')?></li>
                                        <li class="code-lvl-2"><?php echo anchor('test/controladores/favoritas', 'favoritas')?></li>
                                    </ul>
                                </section>
                            </section>
                        </section>
                        <section>
                            <h2 id="section-licencia">Licencia</h2>
                            <p><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">API simple de ejemplo</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="http://pedroescudero.info" property="cc:attributionName" rel="cc:attributionURL">Pedro Escudero</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Reconocimiento-NoComercial-CompartirIgual 4.0 Internacional License</a>. Creado a partir de la obra en <a xmlns:dct="http://purl.org/dc/terms/" href="https://bitbucket.org/escuderopedro/" rel="dct:source">https://bitbucket.org/escuderopedro/</a>.</p>
                            <p>Mas información sobre licencias Creative Commons en <?php echo anchor('http://creativecommons.org/', 'http://creativecommons.org/', array('target'=>'creativecommons')) ?></p>
                        </section>
                    </div>
                </div>

            </div> <!-- /container -->
        </article>
        
        <hr>
        
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-6"><a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Licencia de Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a></div>
                    <div class="col-md-6"><p class="text-align-right">Desarrollado por <a href="http://pedroescudero.info" target="portfolio">Pedro Escudero</a> en enero de 2016</p></div>
                </div>
            </div>
        </footer>

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="/resources/style/js/jquery-1.11.3.min"></script>
        <script src="/resources/style/js/bootstrap.min.js"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="/resources/style/js/ie10-viewport-bug-workaround.js"></script>
    </body>
</html>