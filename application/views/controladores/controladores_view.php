<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="Este es un API de ejemplo desarrollada solo con la intención de tener algo de código que mostrar.">
        <meta name="author" content="">
        <link rel="icon" href="/resources/style/favicon.ico">

        <title>API simple de ejemplo</title>
        
        <!-- CSS Reset -->
        <link href="/resources/style/css/reset.css" rel="stylesheet">

        <!-- Bootstrap core CSS -->
        <link href="/resources/style/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="/resources/style/css/style.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- <link rel="stylesheet" type="text/css" href="print.css" media="print" /> -->
    </head>

    <body>
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <?php echo anchor('/', 'API simple de ejemplo', array('class'=>'navbar-brand')) ?>
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                </div>

                <div class="collapse navbar-collapse" id="bs-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><?php echo anchor(base_url().'#section-licencia', 'Licencia')?></li>
                        <li><?php echo anchor('http://pedroescudero.info', 'Contacto', array('target'=>'portfolio')) ?></li>
                    </ul>
                </div>
            </div>
        </nav>

        <article>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <ol class="breadcrumb">
                            <li><?php echo anchor('notas', 'Notas'); ?></li>
                            <li class="active">Controlador <?php echo $this->uri->segment(3); ?></li>
                        </ol>
                        <h1>Resultado llamada POST</h1>
                        <section>
                            <pre><?php echo htmlentities($respuesta); ?></pre>
                            <?php if($error) { ?>
                            <pre>Error: <?php echo $error; ?></pre>
                            <?php } ?>
                        </section>
                    </div>
                </div>

            </div> <!-- /container -->
        </article>
        
        <hr>
        
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-6"><a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Licencia de Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a></div>
                    <div class="col-md-6"><p class="text-align-right">Desarrollado por <a href="http://pedroescudero.info" target="portfolio">Pedro Escudero</a> en enero de 2016</p></div>
                </div>
            </div>
        </footer>

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="/resources/style/js/jquery-1.11.3.min"></script>
        <script src="/resources/style/js/bootstrap.min.js"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="/resources/style/js/ie10-viewport-bug-workaround.js"></script>
    </body>
</html>