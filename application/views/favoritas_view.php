<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="Este es un API de ejemplo desarrollada solo con la intención de tener algo de código que mostrar.">
        <meta name="author" content="">
        <link rel="icon" href="/resources/style/favicon.ico">

        <title>API simple de ejemplo</title>
        
        <!-- CSS Reset -->
        <link href="/resources/style/css/reset.css" rel="stylesheet">

        <!-- Bootstrap core CSS -->
        <link href="/resources/style/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="/resources/style/css/style.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- <link rel="stylesheet" type="text/css" href="print.css" media="print" /> -->
    </head>

    <body>
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <?php echo anchor('/', 'API simple de ejemplo', array('class'=>'navbar-brand')) ?>
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                </div>

                <div class="collapse navbar-collapse" id="bs-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><?php echo anchor(base_url().'#section-licencia', 'Licencia')?></li>
                        <li><?php echo anchor('http://pedroescudero.info', 'Contacto', array('target'=>'portfolio')) ?></li>
                    </ul>
                </div>
            </div>
        </nav>

        <article>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <ol class="breadcrumb">
                            <li><?php echo anchor('notas', 'Notas'); ?></li>
                            <li class="active">Notas favoritas</li>
                        </ol>
                        <section>
                            <h1>Notas favoritas</h1>
                            <p>Esta llamada se utiliza para gestionar las notas favoritas de un usuario. Pasando el identificador de usuario y opcionalmente una posición inicial y una cantidad de notas a retornar se puede manejar la consulta. En ausencia de estos parámetros, por defecto retorna las 10 últimas notas favoritas.</p>
                            <p>Si se pasa un identificador de nota en lugar de consultar marcará o desmarcará esa nota como favorita, en función de su estado actual.</p>
                            <code><?php echo base_url("notas/favoritas") ?></code>
                            <section>
                                <p><h2>Parámetros</h2></p>
                                <ul>
                                    <li><strong>id_usuario:</strong> Identificador numérico del usuario para el que se quiere crear la nota.</li>
                                    <li><strong>inicial*:</strong> Posición de la primera nota que se retornará. La primera posición es 0. Opcional.</li>
                                    <li><strong>cantidad*:</strong> Número de notas a retornar. Máximo 20. Por defecto 10. Opcional.</li>
                                    <li><strong>id_nota*:</strong> Identificador de la nota, si no está marcada como favorita se marca, si está marcada se desmarca. Este parámetro es prioritario. Opcional.</li>
                                </ul>
                            </section>
                            <section>
                                <p><h2>Retorno</h2></p>
                                <p>Retorna las notas consultadas o la nota marcada/desmarcada como favorita como objeto XML con el siguiente formato.</p>
                                <code>
                                    <p>&lt;respuesta&gt;</p>
                                    <p class="code-lvl-1">&lt;nota&gt;</p>
                                    <p class="code-lvl-2">&lt;id_nota&gt;identificador de la nota&lt;/id_nota&gt;</p>
                                    <p class="code-lvl-2">&lt;texto&gt;texto de la nota&lt;/texto&gt;</p>
                                    <p class="code-lvl-2">&lt;favorita&gt;indicador de si la nota es favorita&lt;/favorita&gt;</p>
                                    <p class="code-lvl-2">&lt;fecha&gt;fecha y hora en la que se creo la nota&lt;/fecha&gt;</p>
                                    <p class="code-lvl-1">&lt;/nota&gt;</p>
                                    <p class="code-lvl-1">&lt;nota&gt;</p>
                                    <p class="code-lvl-2">&lt;id_nota&gt;identificador de la nota&lt;/id_nota&gt;</p>
                                    <p class="code-lvl-2">&lt;texto&gt;texto de la nota&lt;/texto&gt;</p>
                                    <p class="code-lvl-2">&lt;favorita&gt;indicador de si la nota es favorita&lt;/favorita&gt;</p>
                                    <p class="code-lvl-2">&lt;fecha&gt;fecha y hora en la que se creo la nota&lt;/fecha&gt;</p>
                                    <p class="code-lvl-1">&lt;/nota&gt;</p>
                                    <p class="code-lvl-1">&lt;nota&gt;</p>
                                    <p class="code-lvl-2">&lt;id_nota&gt;identificador de la nota&lt;/id_nota&gt;</p>
                                    <p class="code-lvl-2">&lt;texto&gt;texto de la nota&lt;/texto&gt;</p>
                                    <p class="code-lvl-2">&lt;favorita&gt;indicador de si la nota es favorita&lt;/favorita&gt;</p>
                                    <p class="code-lvl-2">&lt;fecha&gt;fecha y hora en la que se creo la nota&lt;/fecha&gt;</p>
                                    <p class="code-lvl-1">&lt;/nota&gt;</p>
                                    <p>&lt;!-- Repetición de notas --&gt;</p>
                                    <p>&lt;/respuesta&gt;</p>
                                </code>
                            </section>
                        </section>
                    </div>
                </div>

            </div> <!-- /container -->
        </article>
        
        <hr>
        
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-6"><a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Licencia de Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a></div>
                    <div class="col-md-6"><p class="text-align-right">Desarrollado por <a href="http://pedroescudero.info" target="portfolio">Pedro Escudero</a> en enero de 2016</p></div>
                </div>
            </div>
        </footer>

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="/resources/style/js/jquery-1.11.3.min"></script>
        <script src="/resources/style/js/bootstrap.min.js"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="/resources/style/js/ie10-viewport-bug-workaround.js"></script>
    </body>
</html>