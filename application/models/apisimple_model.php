<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Apisimple_model extends CI_Model {
	
    function get_notas($id_usuario, $inicial = 0, $cantidad = 10) {
        if($cantidad>20) $cantidad=20;
        return $this->db->select('id, id_usuario, texto, favorita, fecha')
                    ->from('nota')
                    ->where('id_usuario', $id_usuario)
                    ->order_by('fecha', 'desc')
                    ->limit($cantidad, $inicial)
                    ->get();
    }
    
    function get_nota_by_id($id_usuario, $id_nota) {
        return $this->db->select('id, id_usuario, texto, favorita, fecha')
                    ->from('nota')
                    ->where('id_usuario', $id_usuario)
                    ->where('id', $id_nota)
                    ->get();
    }
    
    function get_notas_favoritas($id_usuario, $inicial = 0, $cantidad = 10) {
        if($cantidad>20) $cantidad=20;
        return $this->db->select('id, id_usuario, texto, favorita, fecha')
                    ->from('nota')
                    ->where('id_usuario', $id_usuario)
                    ->where('favorita', '1')
                    ->order_by('fecha', 'desc')
                    ->limit($cantidad, $inicial)
                    ->get();
    }
    
    function set_nota_favorita($id_usuario, $id_nota) {
        $this->db->set('favorita', '!favorita', FALSE);
        $this->db->where('id_usuario', $id_usuario);
        $this->db->where('id', $id_nota);
        $this->db->update('nota');
        
        return $this->get_nota_by_id($id_usuario, $id_nota);
    }
    
    function insert_nota($id_usuario, $texto) {
        $sql_insert = array(
            'id_usuario' => $id_usuario,
            'texto' => $texto
            );
        $this->db->set('fecha', 'NOW()', FALSE);
        $this->db->insert('nota', $sql_insert);
        $id_insertada = $this->db->insert_id();
        
        return $this->get_nota_by_id($id_usuario, $id_insertada);
    }
    
}
